exports.requiresLogin = (req, res, next) ->
  return next() if req.isAuthenticated()
  req.session.returnTo = req.originalUrl if req.method == 'GET'
  res.redirect '/login'

exports.user = hasAuthorization: (req, res, next) ->
  if req.profile.id != req.user.id
    req.flash 'info', 'You are not authorized'
    return res.redirect "/users/#{req.profile.id}"
  next()
