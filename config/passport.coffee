config = require './config'
logger = require('./log4js').logger
mongoose = require 'mongoose'
User = mongoose.model 'User'

#ifdef PASSPORT_LOCAL
LocalStrategy = require('passport-local').Strategy
#endif
#ifdef PASSPORT_TWITTER
TwitterStrategy = require('passport-twitter').Strategy
#endif
#ifdef PASSPORT_FACEBOOK
FacebookStrategy = require('passport-facebook').Strategy
#endif
#ifdef PASSPORT_VKONTAKTE
VkontakteStrategy = require('passport-vkontakte').Strategy
#endif
#ifdef PASSPORT_GITHUB
GitHubStrategy = require('passport-github').Strategy
#endif
#ifdef PASSPORT_GOOGLE
GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
#endif
#ifdef PASSPORT_LINKEDIN
LinkedinStrategy = require('passport-linkedin').Strategy
#endif
#ifdef PASSPORT_MAILRU
MailRuStrategy = require('passport-mailru').Strategy
#endif
#ifdef PASSPORT_YANDEX
YandexStrategy = require('passport-yandex').Strategy
#endif
#ifdef PASSPORT_STEAM
SteamStrategy = require('passport-steam').Strategy
#endif

strategyCallback = exports.strategyCallback =
  (providerName, findQuery, accessToken, refreshToken, profile, done) ->
    return done new Error('Provider is null'), null if !providerName

    User.findOne findQuery, (err, user) ->
      return done err, null if err
      return done err, user if user

      user_query =
        name: profile.displayName
        provider: providerName

      user_query[providerName] = profile._json
      username = profile.username || 'noname'
      email = profile.emails

      user_query['email'] = email[0].value || '' if email && email.length > 0

      if process.env.NODE_ENV == 'development'
        console.error profile

      # TODO: Сделать проверку на типы
      if typeof username != 'string'
        if providerName == 'linkedin'
          username = user_query['email']
        else if providerName == 'yandex'
          username = "#{username}"
        else if providerName == 'steam'
          username = profile.displayName || ''
        else
          username = username[0].value

      user_query['username'] = username
      user = new User user_query

      user.save (err) ->
        return done err, user

exports.init = (passport, config) ->
  passport.serializeUser (user, done) -> done null, user.id

  passport.deserializeUser (id, done) ->
    User.findOne _id:id, (err, user) -> done err, user

#ifdef PASSPORT_LOCAL
  passport.use new LocalStrategy usernameField: 'email', passwordField: 'password',
    (email, password, done) ->
      User.findOne email: email, (err, user) ->
        return done err if err
        return done null, false, message: 'Unknown user' if !user
        return done null, false, message: 'Invalid passport' if !user.authenticate password
        return done null, user
#endif

#ifdef PASSPORT_TWITTER
  if config.twitter.enabled
    passport.use new TwitterStrategy
      consumerKey: config.twitter.clientID
      consumerSecret: config.twitter.clientSecret
      callbackURL: config.twitter.callbackURL
      , (token, tokenSecret, profile, done) ->
        providerName = 'twitter'
        findQuery = {}
        findQuery["#{providerName}.id_str"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_MAILRU
  if config.mailru.enabled
    passport.use new MailRuStrategy
      clientID: config.mailru.clientID
      clientSecret: config.mailru.clientSecret
      clientPrivateKey: config.mailru.clientPrivate
      callbackURL: config.mailru.callbackURL
      , (accessToken, refreshToken, profile, done) ->
        providerName = 'mailru'
        findQuery = {}
        findQuery["#{providerName}.id"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_FACEBOOK
  if config.facebook.enabled
    passport.use new FacebookStrategy
      clientID: config.facebook.clientID
      clientSecret: config.facebook.clientSecret
      callbackURL: config.facebook.callbackURL
      , (accessToken, refreshToken, profile, done) ->
        providerName = 'facebook'
        findQuery = {}
        findQuery["#{providerName}.id"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_VKONTAKTE
  if config.vkontakte.enabled
    passport.use new VkontakteStrategy
      clientID: config.vkontakte.clientID
      clientSecret: config.vkontakte.clientSecret
      callbackURL: config.vkontakte.callbackURL
      , (accessToken, refreshToken, profile, done) ->
        providerName = 'vkontakte'
        findQuery = {}
        findQuery["#{providerName}.id"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_GITHUB
  if config.github.enabled
    passport.use new GitHubStrategy
      clientID: config.github.clientID
      clientSecret: config.github.clientSecret
      callbackURL: config.github.callbackURL
      , (accessToken, refreshToken, profile, done) ->
        providerName = 'github'
        findQuery = {}
        findQuery["#{providerName}.id"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_GOOGLE
  if config.google.enabled
    passport.use new GoogleStrategy
      clientID: config.google.clientID
      clientSecret: config.google.clientSecret
      callbackURL: config.google.callbackURL
      , (accessToken, refreshToken, profile, done) ->
        providerName = 'google'
        findQuery = {}
        findQuery["#{providerName}.id"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_LINKEDIN
  if config.linkedin.enabled
    passport.use new LinkedinStrategy
      consumerKey: config.linkedin.clientID
      consumerSecret: config.linkedin.clientSecret
      callbackURL: config.linkedin.callbackURL
      profileFields: ['id', 'first-name', 'last-name', 'email-address']
      , (accessToken, refreshToken, profile, done) ->
        providerName = 'linkedin'
        findQuery = {}
        findQuery["#{providerName}.id"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_YANDEX
  if config.yandex.enabled
    passport.use new YandexStrategy
      clientID: config.yandex.clientID
      clientSecret: config.yandex.clientSecret
      callbackURL: config.yandex.callbackURL
      , (accessToken, refreshToken, profile, done) ->
        providerName = 'yandex'
        findQuery = {}
        findQuery["#{providerName}.id"] = profile.id
        strategyCallback providerName, findQuery, accessToken, refreshToken, profile, done
#endif

#ifdef PASSPORT_STEAM
  if config.steam.enabled
    steam_params =
      realm: config.steam.realm
      returnURL: config.steam.returnURL

    steam_params['apiKey'] = config.steam.apiKey if config.steam.apiKey
    passport.use new SteamStrategy steam_params, (id, profile, done) ->
      providerName = 'steam'
      findQuery = {}
      findQuery["#{providerName}.id"] = profile.id
      strategyCallback providerName, findQuery, null, null, profile, done
#endif

