
path = require 'path'
ConfigParse = require('../app/utils/configparse')

rootPath = path.normalize "#{__dirname}/.."

node_env = process.env.NODE_ENV || 'development'
callbackHost = 'http://localhost:3000'

all_configs =
  production: {}
  development:
    root: rootPath
    app:
      name: "Demo (dev)"
    server:
      secret: "Save me, superman!"
      database:
        uri: "mongodb://localhost/test"
    providers:
      vkontakte:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/vkontakte/callback"
        image: path: "/img/vkontakte.png"
        authURL: "/auth/vkontakte"                  # optional
        authURLCallback: "/auth/vkontakte/callback" # optional
      facebook:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/facebook/callback"
        image: path: "/img/facebook.png"
      twitter:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/twitter/callback"
        image: path: "/img/twitter.png"
      github:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/github/callback"
        image: path: "/img/github.png"
      google:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/google/callback"
        image: path: "/img/google.png"
      linkedin:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/linkedin/callback"
        image: path: "/img/linkedin.png"
      mailru:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        clientPrivate: "APP_PRIVATE"
        callbackURL: "#{callbackHost}/auth/mailru/callback"
        image: path: "/img/mailru.png"
      yandex:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/yandex/callback"
        image: path: "/img/yandex.png"
      steam:
        apiKey: ""
        realm: "#{callbackHost}/"
        returnURL: "#{callbackHost}/auth/steam/callback"
        image: path: "/img/steam.png"
  test:
    root: rootPath
    app:
      name: "Demo (dev)"
    server:
      secret: "Save me, superman!"
      database:
        uri: "mongodb://localhost/test"
    providers:
      facebook:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/facebook/callback"
        image: path: "/img/facebook.png"
      vkontakte:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/vkontakte/callback"
        image: path: "/img/vkontakte.png"
      twitter:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/twitter/callback"
        image: path: "/img/twitter.png"
      github:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/github/callback"
        image: path: "/img/github.png"
      google:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/google/callback"
        image: path: "/img/google.png"
      linkedin:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/linkedin/callback"
        image: path: "/img/linkedin.png"
      mailru:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        clientPrivate: "APP_PRIVATE"
        callbackURL: "#{callbackHost}/auth/mailru/callback"
        image: path: "/img/mailru.png"
      yandex:
        clientID: "APP_ID"
        clientSecret: "APP_SECRET"
        callbackURL: "#{callbackHost}/auth/yandex/callback"
        image: path: "/img/yandex.png"
      steam:
        apiKey: ""
        realm: "#{callbackHost}/"
        returnURL: "#{callbackHost}/auth/steam/callback"
        image: path: "/img/steam.png"

pconfig = new ConfigParse( all_configs[node_env] )
module.exports = pconfig.json
