users = require '../app/controllers/users'
auth = require './middlewares/authorization'
logger = require('./log4js').logger

module.exports = (app, passport, config) ->
  app.get '/', (req, res) ->
    res.redirect '/login'

  app.get '/login', users.login
  app.get '/signup', users.signup
  app.get '/logout', users.logout

  app.post '/users', users.create
  app.post '/users/session', passport.authenticate('local',
    failureRedirect: '/login', failureFlash: 'Invalid email or password'
  ), users.session

  app.get '/users/:userId', users.show
  app.param 'userId', users.user

#ifndef DISABLED_PASSPORT_ROUTES

# TODO: создавать все маршруты с помощью for() в зависимости от настроек в config.js

#ifdef PASSPORT_MAILRU
  if config.mailru.enabled
    app.get '/auth/mailru', passport.authenticate('mailru', failureRedirect: '/login'), users.signin
    app.get '/auth/mailru/callback', passport.authenticate('mailru', failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_FACEBOOK
  if config.facebook.enabled
    app.get '/auth/facebook', passport.authenticate('facebook',
      failureRedirect: '/login', scope: [ 'email', 'user_about_me']), users.signin
    app.get '/auth/facebook/callback', passport.authenticate('facebook',
      failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_VKONTAKTE
  if config.vkontakte.enabled
    app.get '/auth/vkontakte', passport.authenticate('vkontakte',
      failureRedirect: '/login'), users.signin
    app.get '/auth/vkontakte/callback', passport.authenticate('vkontakte',
      failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_GITHUB
  if config.github.enabled
    app.get '/auth/github', passport.authenticate('github',
      failureRedirect: '/login'), users.signin
    app.get '/auth/github/callback', passport.authenticate('github',
      failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_TWITTER
  if config.twitter.enabled
    app.get '/auth/twitter', passport.authenticate('twitter',
      failureRedirect: '/login'), users.signin
    app.get '/auth/twitter/callback', passport.authenticate('twitter',
      failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_GOOGLE
  if config.google.enabled
    app.get '/auth/google', passport.authenticate('google',
      scope: ['https://www.googleapis.com/auth/userinfo.profile',
              'https://www.googleapis.com/auth/userinfo.email']
      failureRedirect: '/login'), users.signin
    app.get '/auth/google/callback', passport.authenticate('google',
      failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_LINKEDIN
  if config.linkedin.enabled
    app.get '/auth/linkedin', passport.authenticate('linkedin',
      failureRedirect: '/login', scope: [ 'r_emailaddress' ]), users.signin
    app.get '/auth/linkedin/callback', passport.authenticate('linkedin',
      failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_YANDEX
  if config.yandex.enabled
    app.get '/auth/yandex', passport.authenticate('yandex',
      failureRedirect: '/login'), users.signin
    app.get '/auth/yandex/callback', passport.authenticate('yandex',
      failureRedirect: '/login'), users.authCallback
#endif

#ifdef PASSPORT_STEAM
  if config.steam.enabled
    app.get '/auth/steam', passport.authenticate('steam',
      failureRedirect: '/login'), users.signin
    app.get '/auth/steam/callback', passport.authenticate('steam',
      failureRedirect: '/login'), users.authCallback
#endif

#endif

