express = require 'express'
mongoStore = require('connect-mongo')(express)
flash = require 'connect-flash'
helpers = require 'view-helpers'
pkg = require '../package.json'
Log4js = require './log4js'
logger = Log4js.logger
log4js = Log4js.log4js

env = process.env.NODE_ENV || 'development'

module.exports = (app, config, passport) ->
  app.set 'showStackError', true
  app.use express.compress level: 9, filter: (req, res) ->
    return /json|text|javascript|css/.test res.getHeader 'Content-Type'

  app.use express.favicon()
  app.use express.static "#{config.root}/public"

  # app.set 'views', "#{config.root}/app/views"
  # app.set 'view engine', 'ejs'

  ## Nunjucks
  require('nunjucks').configure 'views',
    autoescape: true, express: app

  app.configure () ->
    app.use log4js.connectLogger logger
    app.use (req, res, next) ->
      res.locals.pkg = pkg
      next()

    app.use express.cookieParser()
    app.use express.bodyParser()
    app.use express.methodOverride()

    app.use express.session
      secret: config.server.secret
      store: new mongoStore
        collection: 'sessions'
        url: config.server.database.uri

    app.use passport.initialize()
    app.use passport.session()
    app.use flash()

    app.use helpers pkg.name

    # CSRF
    if process.env.NODE_ENV != 'test'
      app.use express.csrf()
      app.use (req, res, next) ->
        res.locals.csrf_token = req.csrfToken()
        next()

    app.use app.router

    app.use (err, req, res, next) ->
      if err.message && ( ~err.message.indexOf('not found') || ~err.message.indexOf('Cast to ObjectId failed') )
        return next()

      logger.error err.stack if process.env.NODE_ENV == 'development'
      # res.status(500).render('500.html', error: err.stack)
      res.redirect '/'

    app.use (req, res, next) ->
      res.status(404).render '404.html', error: 'Not found', url: req.originalUrl

  app.configure 'development', ->
    app.locals.pretty = true

