log4js = require 'log4js'
log4js.configure "#{__dirname}/logger.json"

env = process.env.NODE_ENV || 'development'

logger_name = 'server'
logger_level = 'INFO'

switch env.toLowerCase()
  when 'test'
    logger_name = 'test'
    logger_level = 'DEBUG'
  when 'development'
    logger_name = 'develop'
  else
    logger_name = 'server'

logger = log4js.getLogger logger_name
logger.setLevel logger_level

module.exports = log4js: log4js, logger: logger
