fs = require 'fs'
express = require 'express'
passport = require 'passport'
logger = require('./config/log4js').logger
config = require('./config/config')
require('./app/db').connect()

models_path = "#{__dirname}/app/models"
fs.readdirSync(models_path).forEach (file) ->
  require "#{models_path}/#{file}" if ~file.indexOf '.js'

app = express()

require('./config/passport').init passport, config['providers']
require('./config/express')(app, config, passport)
require('./config/routes')(app, passport, config['providers'])

app.listen process.env.PORT || 3000, process.env.IP || "0.0.0.0"
exports = module.exports = app
