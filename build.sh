#!/bin/bash

# COFFEE=coffee
COFFEE=./node_modules/coffee-script/bin/coffee

find . -not -path "./node_modules/*" -iname "*.coffee" -exec $COFFEE -bc '{}' \;
