fs = require 'fs'
path = require 'path'
assert = require 'assert'
ConfigParse = require '../../../app/utils/configparse'
test_config_filename =  path.normalize "#{__dirname}/../../_data/config.default.json"

describe 'app::utils::configparse', ->
  describe '-', ->

    beforeEach ->
      data = JSON.parse fs.readFileSync test_config_filename, 'utf8'
      @config = new ConfigParse(data)

    it '#validate (dummy)', (done) ->
      result = @config.validate()
      assert.equal result['ok'], true
      done()
