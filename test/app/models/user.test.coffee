process.env.NODE_ENV = 'test'

mongoose = require 'mongoose'
should = require 'should'
supertest = require 'supertest'
user = require '../../../app/models/user'
app = require '../../../server'
User = mongoose.model 'User'

cookies = null
count = null

describe 'Users', ->
  describe 'POST /users', ->
    before (done) ->
      User.count (err, cnt) ->
        count = cnt
        done()

    it '#no email - should respond with errors', (done) ->
      supertest(app).post '/users'
        .field 'name', 'Foo bar'
        .field 'username', 'foobar'
        .field 'email', ''
        .field 'password', 'foobar'
        .expect 'Content-Type', /html/
        .expect 200
        .expect /Email cannot be blank/
        .end done

    it '#no name - should respond with errors', (done) ->
      supertest(app).post '/users'
        .field 'name', ''
        .field 'username', 'foobar'
        .field 'email', 'foobar@example.com'
        .field 'password', 'foobar'
        .expect 'Content-Type', /html/
        .expect 200
        .expect /Name cannot be blank/
        .end done

    it '#should not save the user to db', (done) ->
      User.count (err, cnt) ->
        count.should.equal cnt
        done()

    describe 'Valid parameters', ->
      before (done) ->
        User.count (err, cnt) ->
          count = cnt
          done()

      it '#should redirect to /', (done) ->
        supertest(app).post '/users'
          .field 'name', 'Foo bar'
          .field 'username', 'foobar'
          .field 'email', 'foobar@example.com'
          .field 'password', 'foobar'
          .expect 'Content-Type', /plain/
          .expect 'Location', /\//
          .expect 302
          .expect /Moved Temporarily/
          .end done

      it '#should insert a record to the db', (done) ->
        User.count (err, cnt) ->
          cnt.should.equal count+1
          done()

      it '#should save the user to the db', (done) ->
        User.findOne(username: 'foobar').exec (err, user) ->
          should.not.exist err
          user.should.be.an.instanceOf User
          user.email.should.equal 'foobar@example.com'
          done()
  after (done) ->
    User.collection.remove done
