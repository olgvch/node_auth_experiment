process.env.NODE_ENV = 'test'

_mongoose = require('mongoose')
_user = require('../../../app/models/user')
_app = require('../../../server')
_User = _mongoose.model('User')

users = require '../../../app/controllers/users'
assert = require 'assert'

describe 'Controllers::users', ->
  describe '- exports', ->

    _test_login = ->
      _run = (req, url_ok) ->
        users.session req, redirect: (url) ->
          assert.equal url, url_ok

      req = session: returnTo: "/my/path"
      _run req, "/my/path"

      req = session: returnTo: undefined
      _run req, "/"

      delete req.session.returnTo
      _run req, "/"

    it '#session', (done) ->
      _test_login()
      done()

    it '#authCallback', (done) ->
      _test_login()
      done()

    it '#signin (dummy)', (done) ->
      done()

    it '#login', (done) ->
      req = flash: (title, msg) -> "FLASH OK"
      users.login req, render: (url, params) ->
        assert.equal url, "users/login.html"
        assert.equal params.title, "Login"
        assert.equal params.message, "FLASH OK"
      done()

    it '#logout', (done) ->
      is_out = false
      is_redirect = false
      req = logout: -> is_out = true
      res = redirect: (url) -> is_redirect = true

      users.logout req, res
      assert.equal is_out, true
      assert.equal is_redirect, true
      done()

    it '#create (dummy)', (done) ->
      # TODO: заМокать User
      done()

    it '#show', (done) ->
      req = profile: name: "Tester"
      res = render: (url, params) ->
        assert.equal url, "users/show.html"
        assert.equal params.title, "Tester"
        assert.equal params.user.name, "Tester"
      users.show req, res
      done()

    it '#user (dummy)', (done) ->
      # TODO: заМокать User
      done()
