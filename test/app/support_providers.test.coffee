assert = require 'assert'
sproviders = require '../../app/support_providers'

describe 'app::support_providers', ->
  it '#isSupportProvider', ->
    for pname in sproviders._providers
      r = sproviders.isSupportProvider pname
      assert.equal r, true

    r = sproviders.isSupportProvider "blablabla"
    assert.equal r, false
