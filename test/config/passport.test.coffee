process.env.NODE_ENV = 'test'

assert = require 'assert'
_JSchema = require 'jayschema'
jayschema = new _JSchema()
mongoose = require 'mongoose'
user = require '../../app/models/user'
User = mongoose.model 'User'
strategyCb = require('../../config/passport').strategyCallback
schemas = require '../_data/schemas.js'
profiles = require '../_data/_profiles.js'

mongoose.connection.removeAllListeners 'disconnected'
require('../../app/db').connect()

describe 'passport', ->
  after (done) ->
    User.collection.remove done

  describe 'test validate profiles', ->
    it '#facebook', ->
      jayschema.validate profiles.facebook, schemas.facebook_schema, (err) ->
        assert.equal typeof err, 'undefined'

    it '#vkontakte', ->
      jayschema.validate profiles.vkontakte, schemas.vkontakte_schema, (err) ->
        assert.equal typeof err, 'undefined'

    it '#mailru', ->
      jayschema.validate profiles.mailru, schemas.mailru_schema, (err) ->
        assert.equal typeof err, 'undefined'

    it '#github', ->
      jayschema.validate profiles.github, schemas.github_schema, (err) ->
        assert.equal typeof err, 'undefined'

    it '#yandex', ->
      jayschema.validate profiles.yandex, schemas.yandex_schema, (err) ->
        assert.equal typeof err, 'undefined'

    it '#steam', ->
      jayschema.validate profiles.steam, schemas.steam_schema, (err) ->
        assert.equal typeof err, 'undefined'

  describe 'strategys', ->
    it '- recursive check strategy providers', (done) ->
      for providerName, data of profiles
        data['provider'] = providerName if providerName == 'steam'
        query = {}
        assert.equal providerName, data.provider
        query["#{providerName}.id"] = data.id
        strategyCb providerName, query, '0000', '0000', data, (err, user) ->
          assert.equal err, null
          assert.notEqual typeof user, null
          user = user[providerName]
          ###
          # TODO: fixme
          assert.notEqual user, undefined
          for ukey, udata of user
            console.log "** #{ukey} -> #{udata}"
            assert.equal udata, data._json[ukey]
          ###
      done()
