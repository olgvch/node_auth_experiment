authorization = require '../../../config/middlewares/authorization.js'
assert = require 'assert'

describe 'Middleware::authorization', ->
  describe '#requiresLogin', ->
    beforeEach (done) ->
      @defaultReturnTo = "/default"
      @req =
        method: "GET"
        originalUrl: "/"
        session: returnTo: @defaultReturnTo
        isAuthenticated: -> true
      @res = redirect: (url) -> url
      @next = -> "ok"
      done()

    it '- isAuthenticated (GET)', (done) ->
      is_ok = false
      @req.isAuthenticated = -> true
      authorization.requiresLogin @req, @res, -> is_ok = true
      assert.equal @req.session.returnTo, @defaultReturnTo
      assert.equal is_ok, true
      done()

    it '- isAuthenticated (not GET)', (done) ->
      is_ok = false
      @req.method = "POST"
      @req.isAuthenticated = -> true
      authorization.requiresLogin @req, @res, -> is_ok = true
      assert.equal @req.session.returnTo, @defaultReturnTo
      assert.equal is_ok, true
      done()

    it '- not isAuthenticated', (done) ->
      is_ok = false
      @req.isAuthenticated = -> false
      @res.redirect = (url) -> is_ok = true
      authorization.requiresLogin @req, @res, ->
        assert.ok false, 'should not be called'
      assert.equal is_ok, true
      done()

  describe '#hasAuthorization', ->
    beforeEach (done) ->
      @req =
        profile: id: 12345
        user: id: 12345
        flash: (title, msg) ->
      @res = redirect: (url) ->
      @next = ->
      done()

    it '- profile.id != user.id', (done) ->
      is_ok = false
      p_id = 1234
      @req.profile.id = p_id
      @req.user.id = 12345678
      @req.flash = (title, msg) ->
        is_ok = true if title == 'info' && msg == 'You are not authorized'
      @res.redirect = (url) ->
        is_ok = ( url == "/users/#{p_id}" )
      authorization.user.hasAuthorization @req, @res, ->
        assert.ok false, 'should not be called'
      assert.equal is_ok, true
      done()

    it '- profile.id == user.id', (done) ->
      is_ok = false
      @req.profile.id = @req.user.id = 12345678
      @req.flash = (title, msg) ->
        assert.ok false, 'should not be called'
      @res.redirect = (url) ->
        assert.ok false, 'should not be called'
      authorization.user.hasAuthorization @req, @res, -> is_ok = true
      assert.equal is_ok, true
      done()
