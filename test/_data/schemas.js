// TODO: Сделать проверку подробнее

exports.facebook_schema = {
    "title": "Facebook",
    "type": "object",
    "properties": {
        "id": { "type": "string" },
        "username": { "type": "string" },
        "displayName": { "type": "string" },
        "name": {
            "type": "object",
            "properties": {
                "familyName": { "type": [ "string", "undefined" ] },
                "givenName": { "type": [ "string", "undefined" ] },
                "middleName": { "type": [ "string", "undefined" ] }
            },
            "required": ["familyName", "givenName", "middleName"]
        },
        "gender": { "type": "string", "enum": [ "male", "female" ] },
        "profileUrl": { "type": "string" },
        "provider": { "type": "string", "enum": [ "facebook" ] },
        "_raw": { "type": "string" },
        "_json": {
            "type": "object",
            "properties": {
                "id": { "type": "string" }
                , "name": { "type": "string" }
                , "first_name": { "type": "string" }
                , "last_name": { "type": "string" }
                , "link": { "type": "string" }
                , "gender": { "type": "string", "enum": [ "male", "female" ] }
                , "timezone": { "type": "number" }
                , "locale": { "type": "string" }
                , "verified": { "type": "boolean" }
                , "updated_time": { "type": "string" }
                , "username": { "type": "string" }
            }
        }
    }
};

exports.github_schema = {
    "title": "Github",
    "type": "object",
    "properties": {
        "provider": { "type": "string", "enum": [ "github" ] },
        "id": { "type": "number" },
        "displayName": { "type": "string" },
        "username": { "type": "string" },
        "profileUrl": { "type": "string" },
        "emails": {
            "type": "array",
            "properties": {
                // TODO: Сделать проверку на асс-массивы: { value: ''}, ...
                "type": "object"
            }
        },
        "_raw": { "type": "string" },
        "_json": {
            "type": "object"
            , "properties": {
                "login": { "type": "string" }
                , "id": { "type": "number" }
                , "avatar_url": { "type": "string" } // URL
                , "gravatar_id": { "type": "string", "pattern": "^[0-9a-h]{32}$" }
                , "url": { "type": "string" } // URL
                , "html_url": { "type": "string" } // URL
                , "followers_url": { "type": "string" } // URL
                , "following_url": { "type": "string" } // URL
                , "gists_url": { "type": "string" } // URL
                , "starred_url": { "type": "string" } // URL
                , "subscriptions_url": { "type": "string" } // URL
                , "organizations_url": { "type": "string" } // URL
                , "repos_url": { "type": "string" } // URL
                , "events_url": { "type": "string" } // URL
                , "received_events_url": { "type": "string" } // URL
                , "type": { "type": "string" }
                , "site_admin": { "type": "boolean" }
                , "name": { "type": "string" }
                , "company": { "type": "string" }
                , "blog": { "type": "string" }
                , "location": { "type": "string" }
                , "email": { "type": "string" }
                , "hireable": { "type": "boolean" }
                , "bio": { "type": [ "string", "object", "null" ] }
                , "public_repos": { "type": "number" }
                , "public_gists": { "type": "number" }
                , "followers": { "type": "number" }
                , "following": { "type": "number" }
                , "created_at": { "type": "string" } //? '2014-01-01T11:08:59Z'
                , "updated_at": { "type": "string" }
            }
        }
    }
};

exports.vkontakte_schema = {
    "title": "VKontakte",
    "type": "object",
    "properties": {
        "id": { "type": "number" },
        "username": { "type": "string" },
        "displayName": { "type": "string" },
        "name": {
            "type": "object",
            "properties": {
                "familyName": { "type": "string" },
                "givenName": { "type": "string" }
            }
        },
        "gender": { "type": "string", "enum": [ "male", "female" ] },
        "profileUrl": { "type": "string" },
        "photos": {
            "type": "array",
            "properties": {
                "type": "object"
                //, "properties": {
                //    "value": { "type": "string" },
                //    "type": { "type": "string" }
                //}
            }
        },
        "provider": { "type": "string", "enum": [ "vkontakte" ] },
        "_raw": { "type": "string" },
        "_json": {
            "type": "object",
            "properties": {
                "id": { "type": "number" }
                , "first_name": { "type": "string" }
                , "last_name": { "type": "string" }
                , "sex": { "type": "number" }
                , "screen_name": { "type": "string" }
                , "photo": { "type": "string" }
            }
        }
    }
};

exports.mailru_schema = {
    "title": "Mailru",
    "type": "object",
    "properties": {
        "provider": { "type": "string", "enum": [ "mailru" ] },
        "id": { "type": "string", "pattern": "^[0-9]+$" }, // [0-9]
        "username": { "type": "string" },
        "displayName": { "type": "string" },
        "name": {
            "type": "object",
            "properties": {
                "familyName": { "type": "string" },
                "givenName": { "type": "string" }
            }
        },
        "profileUrl": { "type": "string" },
        "emails": {
            "type": "array",
            "properties": {
                "type": "object",
                "properties": {
                    "value": { "type": [ "string", "undefined" ] }
                }
            }
        },
        "_raw": { "type": "string" },
        "_json": {
            "type": "object",
            "properties": {
                "pic_50": { "type": "string" } // URL
                , "friends_count": { "type": "number" }
                , "pic_22": { "type": "string" } // URL
                , "show_age": { "type": "number" }
                , "nick": { "type": "string" }
                , "age": { "type": "number" }
                , "is_friend": { "type": "number" }
                , "is_online": { "type": "number" }
                , "pic_big": { "type": "string" } // URL
                , "is_verified": { "type": "number" }
                , "last_name": { "type": "string" }
                , "has_pic": { "type": "number" }
                , "pic_190": { "type": "string" } // URL
                , "referer_id": { "type": "string" }
                , "vip": { "type": "number" }
                , "birthday": { "type": "string", "pattern": "^[0-9]{2}.[0-9]{2}.[0-9]{4}$" } // '01.01.1911'
                , "pic_32": { "type": "string" } // URL
                , "referer_type": { "type": "string" }
                , "link": { "type": "string" } // URL
                , "last_visit": { "type": "string", "pattern": "^[0-9]+$" } //'1395260962'
                , "uid": { "type": "string", "pattern": "^[0-9]+$" } //'20458694286114043200'
                , "app_installed": { "type": "number" }
                , "status_text": { "type": "string" }
                , "pic_128": { "type": "string" } // URL
                , "sex": { "type": "number" }
                , "pic_small": { "type": "string" } // URL
                , "pic": { "type": "string" } // URL
                , "pic_180": { "type": "string" } // URL
                , "pic_40": { "type": "string" } // URL
                , "first_name": { "type": "string" }
            }
        }
    }
};

exports.yandex_schema = {
    "title": "Yandex",
    "type": "object",
    "properties": {
        "provider": { "type": "string", "enum": [ "yandex" ] },
        "id": { "type": "string", "pattern": "^[0-9]+$" },
        "username": { "type": [ "string", "undefined" ] },
        "displayName": { "type": [ "string", "undefined" ] },
        "name": { "type": "object" },
        "gender": { "type": [ "string", "undefined" ] },
        "emails": {
            "type": "array",
            "properties": {
                "type": "object",
                "properties": {
                    "value": { "type": [ "undefined", "string" ] }
                }
            }
        },
        "_raw": { "type": "string" },
        "_json": { "type": "object" }
    }
};

exports.steam_schema = {
    "title": "Steam",
    "type": "object",
    "properties": {
        "displayName": { "type": [ "string", "undefined" ] },
        "emails": {
            "type": "array",
            "properties": {
                "type": "object",
                "properties": {
                    "value": { "type": [ "string", "undefined" ] }
                }
            }
        },
        "name": {
            "type": "object",
            "properties": {
                "type": "object",
                "familyName": { "type": [ "string", "undefined" ] },
                "givenName": { "type": [ "string", "undefined" ] }
            }
        }
    }
};

exports.db_userschema_provider = {
    "title": "db userschema provider",
    "type": "object",
    "properties": {
        "id": { "type": "string" }
        , "name": { "type": "string" }
        , "first_name": { "type": "string" }
        , "last_name": { "type": "string" }
        , "link": { "type": "string" }
        , "gender": { "type": "string", "enum": [ "male", "female" ] }
        , "timezone": { "type": "number" }
        , "locale": { "type": "string" }
        , "verified": { "type": "boolean" }
        , "updated_time": { "type": "string" } // 2014-03-18T16:29:25+0000
        , "username": { "type": "string" }
    }
};

