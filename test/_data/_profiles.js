
module.exports = {

    facebook: {
        id: '12345678',
        username: 'ivan.ivanov.12312',
        displayName: 'Ivan Ivanov',
        name: {
            familyName: 'Ivanov',
            givenName: 'Ivan',
            middleName: undefined
        },
        gender: 'male',
        profileUrl: 'http://facebook.com/ivan.ivanov.12312',
        provider: 'facebook',
        _raw: '{"id":"12345678","name":"Ivan Ivanov","first_name":"Ivan","last_name":"Ivanov","link":"http:\\/\\/www.facebook.com\\/ivan.ivanov.12312","gender":"male","timezone":6,"locale":"ru_RU","verified":true,"updated_time":"2014-03-18T16:29:25+0000","username":"ivan99"}',
        _json: {
            id: '12345678',
            name: 'Valentin Ivanov',
            first_name: 'Valentin',
            last_name: 'Ivanov',
            link: 'http://facebook.com/ivan.ivanov.12312',
            gender: 'male',
            timezone: 6,
            locale: 'ru_RU',
            verified: true,
            updated_time: '2014-03-18T16:29:25+0000',
            username: 'ivan99'
        }
    },

    github: {
        provider: 'github',
        id: 12345679,
        displayName: 'Ivan',
        username: 'ivanichanon',
        profileUrl: 'https://github.com/ivanichanon',
        emails: [ { value: '' } ],
        _raw: '{"login":"ivanichanon","id":12345679,"avatar_url":"https://avatars.githubusercontent.com/u/12345679?","gravatar_id":"0ac113ffe5250880076ef9679efe1ae1","url":"https://api.github.com/users/ivanichanon","html_url":"https://github.com/ivanichanon","followers_url":"https://api.github.com/users/ivanichanon/followers","following_url":"https://api.github.com/users/ivanichanon/following{/other_user}","gists_url":"https://api.github.com/users/ivanichanon/gists{/gist_id}","starred_url":"https://api.github.com/users/ivanichanon/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ivanichanon/subscriptions","organizations_url":"https://api.github.com/users/ivanichanon/orgs","repos_url":"https://api.github.com/users/ivanichanon/repos","events_url":"https://api.github.com/users/ivanichanon/events{/privacy}","received_events_url":"https://api.github.com/users/ivanichanon/received_events","type":"User","site_admin":false,"name":"Ivan","company":"","blog":"","location":"","email":"","hireable":false,"bio":null,"public_repos":50,"public_gists":0,"followers":0,"following":1,"created_at":"2014-01-01T11:08:59Z","updated_at":"2014-03-19T19:30:03Z"}',
        _json:
        { login: 'mrlnnr',
            id: 12345679,
            avatar_url: 'https://avatars.githubusercontent.com/u/12345679?',
            gravatar_id: '0ac113ffe5250880076ef9679efe1ae1',
            url: 'https://api.github.com/users/ivanichanon',
            html_url: 'https://github.com/ivanichanon',
            followers_url: 'https://api.github.com/users/ivanichanon/followers',
            following_url: 'https://api.github.com/users/ivanichanon/following{/other_user}',
            gists_url: 'https://api.github.com/users/ivanichanon/gists{/gist_id}',
            starred_url: 'https://api.github.com/users/ivanichanon/starred{/owner}{/repo}',
            subscriptions_url: 'https://api.github.com/users/ivanichanon/subscriptions',
            organizations_url: 'https://api.github.com/users/ivanichanon/orgs',
            repos_url: 'https://api.github.com/users/ivanichanon/repos',
            events_url: 'https://api.github.com/users/ivanichanon/events{/privacy}',
            received_events_url: 'https://api.github.com/users/ivanichanon/received_events',
            type: 'User',
            site_admin: false,
            name: 'Ivan',
            company: '',
            blog: '',
            location: '',
            email: '',
            hireable: false,
            bio: null,
            public_repos: 50,
            public_gists: 0,
            followers: 0,
            following: 1,
            created_at: '2014-01-01T11:08:59Z',
            updated_at: '2014-03-19T19:30:03Z'
        }
    },

    vkontakte: {
        id: 1231,
        username: 'i_ivanov_v',
        displayName: 'Ivan Ivanov',
        name: { familyName: 'Ivanov', givenName: 'Ivan' },
        gender: 'male',
        profileUrl: 'http://vk.com/iivan',
        photos:
            [
                {
                    value: 'http://cs1231.vk.me/v313528107/1231/YUE3f4o4gIs.jpg',
                    type: 'photo'
                }
            ],
        provider: 'vkontakte',
        _raw: '{"response":[{"id":1231,"first_name":"Ivan","last_name":"Ivanov","sex":2,"screen_name":"iivan","photo":"http:\\/\\/cs1231.vk.me\\/v313528107\\/1231\\/YUE3f4o4gIs.jpg"}]}',
        _json: {
            id: 1231,
            first_name: 'Ivan',
            last_name: 'Ivanov',
            sex: 2,
            screen_name: 'iivan',
            photo: 'http://cs1231.vk.me/v313528107/1231/YUE3f4o4gIs.jpg'
        }
    },

    mailru: {
        provider: 'mailru',
        id: '23458699286114043200',
        username: 'Иван Иванов',
        displayName: 'Иван Иванов',
        name: { familyName: 'Иванов', givenName: 'Иван' },
        profileUrl: 'http://my.mail.ru/mail/ivan.ivanov0101/',
        emails: [ { value: undefined } ],
        _raw: '[{"pic_50":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar50","friends_count":0,"pic_22":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar22","show_age":1,"nick":"Иван Иванов","age":12,"is_friend":0,"is_online":0,"pic_big":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatarbig","is_verified":1,"last_name":"Иванов","has_pic":0,"pic_190":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar190","referer_id":"","vip":0,"birthday":"01.01.1911","pic_32":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar32","referer_type":"","link":"http://my.mail.ru/mail/ivan.ivanov0101/","last_visit":"1395260962","uid":"23458699286114043200","app_installed":1,"status_text":"","pic_128":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar128","sex":0,"pic_small":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatarsmall","pic":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar","pic_180":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar180","pic_40":"http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar40","first_name":"Иван"}]',
        _json:
        {
            pic_50: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar50',
            friends_count: 0,
            pic_22: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar22',
            show_age: 1,
            nick: 'Иван Иванов',
            age: 12,
            is_friend: 0,
            is_online: 0,
            pic_big: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatarbig',
            is_verified: 1,
            last_name: 'Иванов',
            has_pic: 0,
            pic_190: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar190',
            referer_id: '',
            vip: 0,
            birthday: '01.01.1911',
            pic_32: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar32',
            referer_type: '',
            link: 'http://my.mail.ru/mail/ivan.ivanov0101/',
            last_visit: '1395260962',
            uid: '23458699286114043200',
            app_installed: 1,
            status_text: '',
            pic_128: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar128',
            sex: 0,
            pic_small: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatarsmall',
            pic: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar',
            pic_180: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar180',
            pic_40: 'http://avt.appsmail.ru/mail/ivan.ivanov0101/_avatar40',
            first_name: 'Иван'
        }
    },

    yandex: {
        provider: 'yandex',
        id: '12345678',
        username: undefined,
        displayName: undefined,
        name: {},
        gender: undefined,
        emails: [ { value: undefined } ],
        _raw: '{"id": "12345678"}',
        _json: { id: '12345678' }
    },

    steam: {
        displayName: undefined,
        emails: [ { value: undefined } ],
        name: { familyName: undefined, givenName: undefined }
    }
}
