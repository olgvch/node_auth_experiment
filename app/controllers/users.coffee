mongoose = require 'mongoose'
User = mongoose.model 'User'
utils = require '../../lib/utils'

login = (req, res) ->
  redirectTo = if req.session.returnTo then req.session.returnTo else "/"
  delete req.session.returnTo
  res.redirect redirectTo

exports.session = login
exports.authCallback = login
exports.signin = (req, res) ->

exports.login = (req, res) ->
  res.render 'users/login.html', title: 'Login', message: req.flash 'error'

exports.logout = (req, res) ->
  req.logout()
  res.redirect '/login'

exports.signup = (req, res) ->
  res.render 'users/signup.html', title: 'Sign up', user: new User()

exports.create = (req, res, next) ->
  user = new User req.body
  user.provider = 'local'

  user.save (err) ->
    if err
      return res.render 'users/signup.html', title: 'Sign up', user: user, error: utils.errors err.errors

    ### manually login the user once successfully signed up ###
    req.logIn user, (err) ->
      return next err if err
      return res.redirect '/'

exports.show = (req, res) ->
  user = req.profile
  res.render 'users/show.html', title: user.name, user: user

exports.user = (req, res, next, id) ->
  User.findOne( _id: id ).exec (err, user) ->
    return next err if err
    return next new Error "Failed to load User #{id}" if !user
    req.profile = user
    next()
