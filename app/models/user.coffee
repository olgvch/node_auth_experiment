mongoose = require 'mongoose'
Schema = mongoose.Schema
crypto = require 'crypto'

oAuthTypes = exports.oAuthTypes = require('../support_providers')._providers

UserSchema = new Schema
  name: type: String, default: ''
  email: type: String, default: ''
  username: type: String, default: ''
  provider: type: String, default: ''
  hashed_password: type: String, default: ''
  salt: type: String, default: ''
  authToken: type: String, default: ''
  facebook: {}
  vkontakte: {}
  twitter: {}
  github: {}
  google: {}
  linkedin: {}
  mailru: {}
  yandex: {}
  steam: {}

UserSchema.virtual 'password'
.set (password) ->
  @_password = password
  @salt = @makeSalt()
  @hashed_password = @encryptPassword password
.get -> @_password

validatePresenceOf = (value) ->
  value && value.length

UserSchema.path 'name'
.validate (name) ->
  return true if @doesNotRequireValidation()
  return name.length
, 'Name cannot be blank'

UserSchema.path 'email'
.validate (email) ->
  return true if @doesNotRequireValidation()
  return email.length
, 'Email cannot be blank'

UserSchema.path 'email'
.validate (email, fn) ->
  User = mongoose.model 'User'
  return fn true if @doesNotRequireValidation()

  if @isNew || @isModified 'email'
    User.find(email: email).exec (err, users) ->
      fn !err && users.length == 0
  else
    fn true
, 'Email already exists'

UserSchema.path 'username'
.validate (username) ->
  return true if @doesNotRequireValidation()
  return username.length
, 'Username cannot be blank'

UserSchema.path 'hashed_password'
.validate (hashed_password) ->
  return true if @doesNotRequireValidation()
  return hashed_password.length
, 'Password cannot be blank'

UserSchema.pre 'save', (next) ->
  return next() if !@isNew
  if !validatePresenceOf(@password) && !@doesNotRequireValidation()
    next new Error 'Invalid password'
  else
    next()

UserSchema.methods =
  authenticate: (plainText) ->
    return @encryptPassword plainText == @hashed_password

  makeSalt: ->
    ### Math.round((new Date().valueOf() * Math.random()))+'' ###
    crypto.randomBytes(16).toString('base64')

  encryptPassword: (password) ->
    return '' if !password
    try
      return crypto.createHmac('sha1', @salt).update(password).digest('hex')
    catch
      return ''

  doesNotRequireValidation: ->
    ~oAuthTypes.indexOf(@provider)

mongoose.model 'User', UserSchema
