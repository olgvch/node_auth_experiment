isSupportProvider = require('../support_providers').isSupportProvider
logger = require('../../config/log4js').logger

urlClientIdPattern = /^[0-9]+$/i
urlClientSecretPattern = /^[0-9a-z]+$/i
urlClientPrivatePattern = /^[0-9a-z]+$/i
urlCallbackPattern = /^(http|https):\/\/([a-z_\-\.]+)(:[0-9]{2,5})?\/auth\/(\w+)\/callback$/i
realmPattern = /^(http|https):\/\/([a-z_\-\.]+)(:[0-9]{2,5})?\/$/i

allowed_parameters = [
  'realm', 'clientID', 'clientSecret',
  'clientPrivate', 'callbackURL', 'image',
  'authURL', 'authURLCallback'
]

class ConfigParse
  constructor: (@json) -> @_checkProviders()

ConfigParse::validate = ->
  ### TODO: Сделать нормальную проверку ###

  _return = (msg="") -> ok: msg == "", err: msg

  return _return("Key 'root' not found!") if (!@json['root'])
  return _return("Key 'server' not found!") if (!@json['server'])
  return _return("Key 'server.secret' not found!") if (!@json['server']['secret'])
  return _return("Key 'server.database' not found!") if (!@json['server']['database'])
  return _return("Key 'server.database.uri' not found!") if (!@json['server']['database']['uri'])

  _return()

ConfigParse::_checkProviders = ->
  ### Удаляет все лишние соцсети, у которые некорректные настройки ###

  # _remove_last_sep = (s) -> s.substr(0, s.length-1) if s.charAt(s.length-1) == '/'

  if !@json["providers"]
    @json["providers"] = {}
  else
    new_providers = {}
    for key, value of @json["providers"]
      continue if ! isSupportProvider key

      # если true, то игнорируем провайдера, прописывая enabled=false
      skip_provider = false

      # проверка на обязательные параметры
      for pname in Object.keys(value) || []
        if allowed_parameters.indexOf(pname) < 0
          logger.warn "PROVIDER:config '#{key}' unknown parameter: '#{pname}'"
          skip_provider = true
          continue

      if skip_provider != false
        new_providers[key] = enabled: false
        logger.warn "PROVIDER:config '#{key}' REMOVED and DISABLED !!!"
        continue

      is_enabled = false
      image = size: "32x32", path: null

      _authUrl = "/auth/#{key}"
      _authUrlCallback = "#{_authUrl}/callback"

      clientId = if urlClientIdPattern.test(value["clientID"]) then value["clientID"] else undefined
      clientSecret = if urlClientSecretPattern.test(value["clientSecret"]) then value["clientSecret"] else undefined
      clientPrivate = if urlClientPrivatePattern.test(value["clientPrivate"]) then value["clientPrivate"] else undefined
      callbackUrl = if urlCallbackPattern.test(value["callbackURL"]) then value["callbackURL"] else undefined

      authURL = if value["authURL"] then value["authURL"] else _authUrl
      authURL = authURL.substr(0, authURL.length-1) if authURL.charAt(authURL.length-1) == '/'

      authURLCallback = if value["authURLCallback"] then value["authURLCallback"] else _authUrlCallback
      authURLCallback = authURL.substr(0, authURLCallback.length-1) if authURLCallback.charAt(authURLCallback.length-1) == "/"

      _realm = value["realm"]
      _realm = "#{_realm}/" if _realm && _realm.charAt(_realm.length-1) != '/'

      realm = if realmPattern.test(_realm) then _realm else undefined
      is_enabled = true if clientId && clientSecret && callbackUrl && authURL

      if value['image']
        image['path'] = value['image']['path'] if value['image']['path']
        image['size'] = value['image']['size'] if value['image']['size']

      _msg_prefix = if is_enabled then "EN" else "DIS"
      logger.warn "PROVIDER:config '#{key}' is valid and #{_msg_prefix}ABLED"

      new_providers[ key ] =
        enabled: is_enabled
        clientID: clientId
        clientSecret: clientSecret
        clientPrivate: clientPrivate
        callbackURL: callbackUrl
        authURL: authURL
        authURLCallback: authURLCallback
        realm: realm
        image: image

    @json["providers"] = new_providers

module.exports = ConfigParse
