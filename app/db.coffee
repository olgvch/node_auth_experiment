env = process.env.NODE_ENV || "development"
config = require('../config/config')
mongoose = require 'mongoose'
logger = require('../config/log4js').logger

connect = (mongo_uri) ->
  options = server: socketOptions: keepAlive: 1
  mongoose.connect mongo_uri || config.server.database.uri, options

### error handler ###
mongoose.connection.on 'error', (err) ->
  logger.error "mongoose.connection::error: #{err.message}"

### reconnect ###
mongoose.connection.on 'disconnected', () ->
  connect()

exports.connect = connect
