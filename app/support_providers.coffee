_providers = exports._providers = [
  'vkontakte'
  'facebook'
  'mailru'
  'twitter'
  'github'
  'google'
  'linkedin'
  'yandex'
  'steam'
]

exports.isSupportProvider = (name) ->
  _providers.indexOf(name) >= 0
