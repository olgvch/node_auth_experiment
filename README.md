Install
-------

```bash
$ npm i
$ cp config/config.example.coffee config/config.coffee
$ sh ./build.sh
$ npm test && npm start
```
